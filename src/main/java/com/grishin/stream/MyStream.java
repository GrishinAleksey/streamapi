package com.grishin.stream;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyStream<T> {

    private final List<Object> actions;
    private final List<T> data;

    public MyStream(List<? extends T> data) {
        this.data = new ArrayList<>(data);
        this.actions = new ArrayList<>();
    }

    /**
     * Статический метод, который принимает коллекцию и создает новый объект MyStreamImpl.
     *
     * @param data коллекция
     * @return новый объект MyStreamImpl
     */
    @SafeVarargs
    public static <T> MyStream<T> of(T... data) {
        return new MyStream<>(Arrays.asList(data));
    }

    /**
     * Добавляет в коллекцию actions predicate-условие
     *
     * @param predicate условие
     * @return новый объект MyStream
     */
    public MyStream<T> filter(Predicate<? super T> predicate) {
        actions.add(predicate);
        return this;
    }

    /**
     * Добавляет в коллекцию actions function-преобразование
     *
     * @param function преобразование
     * @return новый объект MyStream
     */
    public <R> MyStream<R> transform(Function<? super T, ? extends R> function) {
        actions.add(function);
        return (MyStream<R>) this;
    }

    /**
     * Проверяет удовлетворяет ли элемент условию в лямбде и возвращает boolean.
     *
     * @param t проверяемый параметр
     * @param object объект
     * @return boolean результат
     */
    public boolean checkPredicate(T t, Object object) {
        if (object instanceof Predicate) {
            Predicate predicate = (Predicate) object;
            return !predicate.test(t);
        }
        return false;
    }

    /**
     * Преобразует элемент в другой, заданный в условии элемент и возвращает измененный объект.
     *
     * @param t проверяемый параметр
     * @param object объект
     * @return измененный объект t
     */
    public T checkFunction(T t, Object object) {
        if (object instanceof Function) {
            Function function = (Function) object;
            t = (T) function.apply(t);
        }
        return t;
    }

    /**
     * Принимает 2 лямбды для создания мапы, в одной указывается, что использовать в качестве ключа, в другой, что в качестве значения. Терминальная операция.
     *
     * @param val первая лямбда-значение
     * @param key вторая лямбда-ключ
     * @return новый объект MyStreamImpl
     */
    public <K, V> Map<K, V> toMap(Function<? super T, ? extends K> val, Function<? super T, ? extends V> key) {
        Map<K, V> resultMap = new HashMap<>();
        for (T t : data) {
            for (Object o : actions) {
                if (checkPredicate(t, o)) break;
                t = checkFunction(t, o);
                resultMap.put(val.apply(t), key.apply(t));
            }
        }
        return resultMap;
    }
}