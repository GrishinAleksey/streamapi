package com.grishin;

import com.grishin.entity.Person;
import com.grishin.stream.MyStream;

import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(new Person("Андрей", 1), new Person("Алексей", 2), new Person("Антон", 3));
        System.out.println(new MyStream<>(people)
                .filter(p -> p.getId() > 2)
                .transform(p -> new Person("qq", p.getId() + 30))
                .toMap(Person::getName, p -> p));
    }
}
